from tkinter import Tk, Frame, Label, Entry, Button, messagebox
import tkinter as tk
import mysql.connector
fonts = ('Courier New', 20, 'bold')
# Create a connection to the MySQL database
db_connection = mysql.connector.connect(
    host="localhost",
    user="root",
    password="Anjali@2004",
    database="voters_list"
)
# Create a dictionary to store candidate names and their vote counts
candidates = {"Candidate A": 0, "Candidate B": 0, "Candidate C": 0}

# Create a global variable for the current user
current_user = None
voted_users = set()
#logged_in_users = set()

class Login:
    def __init__(self, root) -> None:
        self.root = root
        self.frame = Frame(self.root, width= 550, height= 350, bg = 'white', bd= 4 , relief= 'ridge')
        self.frame.place(x = 0, y = 0)

        self.name_lbl = Label(self.frame, text = 'NAME', bg = 'white', fg = 'steel blue', font=fonts)
        self.name_lbl.place(x = 20, y = 50)

        self.pass_lbl = Label(self.frame, text = 'PASSWORD', bg = 'white', fg = 'steel blue', font=fonts)
        self.pass_lbl.place(x = 20, y = 100)

        self.name_entry = Entry(self.frame, font= fonts, bg = 'white')
        self.name_entry.place(x = 200 , y = 50)
        self.pass_entry = Entry(self.frame, font= fonts, show= '*',bg = 'white')
        self.pass_entry.place(x = 200 , y = 100)

        self.submit_btn = Button(self.frame, text = 'LOGIN', bg = 'white', fg = 'steel blue', font= fonts, command= self.authenticate)
        self.submit_btn.place(x = 250, y = 250)


    def authenticate(self):
        username = self.name_entry.get()
        password = self.pass_entry.get()
        cursor = db_connection.cursor()
        cursor.execute('select * from V;') 
        data = cursor.fetchall()
        names = [item[0] for item in data]
        passw = [pas[1] for pas in data]
        if username in names and password in passw and names.index(username)==passw.index(password):
            save = password
            current_user=username
            messagebox.showinfo("Login Successful", "Welcome, " + username+"!")
            self.frame.destroy()
            Display_home_page()
        else:
            messagebox.showerror("Login Error", "Invalid username or password.")
# Function to display the home page with voting and analysis options
def Display_home_page():

    root = tk.Tk()
    root.title("Online Voting System")

    # Voting options
    vote_buttons = []
    for i, candidate in enumerate(candidates, start=1):
        vote_button = tk.Button(root, text=f"Vote for {candidate}", command=lambda c=candidate: vote(c))
        vote_button.place(x=20, y=20 + (i - 1) * 30)
        vote_buttons.append(vote_button)

    # Analysis button
    analyze_button = tk.Button(root, text="Analyze Votes", command=show_analysis)
    analyze_button.place(x=20, y=20 + len(candidates) * 30)



    root.mainloop()


# Function to handle the voting process
def vote(candidate):
    global current_user
    if current_user in voted_users:
        messagebox.showerror("Error", "You have already voted.")
    else:
        candidates[candidate] += 1
        voted_users.add(current_user)
        messagebox.showinfo("Success", f"You have voted for {candidate}. Thank you!")
        


# Function to display the vote counts and user statistics in a separate window
def show_analysis():
    analysis_window = tk.Toplevel()
    analysis_window.title("Vote Analysis")

    analysis_text = tk.Text(analysis_window, height=5, width=30)
    analysis_text.place(x=20, y=10)

    analysis_text.config(state=tk.NORMAL)

    total_users = len(voted_users)
    analysis_text.insert(tk.END, f"Total Users Logged In: {total_users}\n")

    for candidate, count in candidates.items():
        analysis_text.insert(tk.END, f"{candidate}: {count} votes\n")
    analysis_text.config(state=tk.DISABLED)


root = tk.Tk()

root.title('LOGIN-PAGE')
root.geometry('550x350+550+200')
root.resizable(False,False)
login = Login(root)
root.mainloop()