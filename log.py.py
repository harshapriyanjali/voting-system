import tkinter as tk
from tkinter import messagebox

# Global variables
candidates = {"Candidate A": 0, "Candidate B": 0, "Candidate C": 0}
voted_users = set()
current_user = None

# List of static users
users = [
    {"username": "user1", "password": "password1"},
    {"username": "user2", "password": "password2"},
    {"username": "user3", "password": "password3"}
]
current_user_index = 0  # Index to track current user

def authenticate(username, password):
    # Check provided username and password against the list of users
    for user in users:
        if user["username"] == username and user["password"] == password:
            return True
    return False

def on_login():
    global current_user, current_user_index
    username = username_entry.get()
    password = password_entry.get()
    if authenticate(username, password):
        if username in voted_users:
            messagebox.showerror("Error", "You have already voted.")
            return
        current_user = username
        messagebox.showinfo("Success", "Login successful!")
        login_frame.grid_forget()  # Hide login frame after successful login
        vote_button["state"] = tk.NORMAL  # Enable vote button
        results_button["state"] = tk.NORMAL  # Enable results button
        vote_frame.grid(row=1, column=0, padx=20, pady=20)  # Show vote frame
    else:
        messagebox.showerror("Error", "Invalid username or password!")

def vote(candidate):
    global current_user_index
    candidates[candidate] += 1
    voted_users.add(current_user)
    messagebox.showinfo("Success", f"You have voted for {candidate}. Thank you!")
    current_user_index += 1
    if current_user_index >= len(users):
        messagebox.showinfo("Voting Completed", "All users have voted.")
        vote_button["state"] = tk.DISABLED  # Disable vote button after all users have voted
    else:
        login_frame.grid(row=1, column=0, padx=20, pady=20)  # Show login frame for the next user
        vote_button["state"] = tk.DISABLED  # Disable vote button after voting
        vote_frame.grid_forget()  # Hide vote frame after voting

def show_results():
    results = "Results:\n"
    for candidate, votes in candidates.items():
        results += f"{candidate}: {votes}\n"
    messagebox.showinfo("Results", results)

# Main window
root = tk.Tk()
root.title("Voting System")

# Create buttons using grid()
button_frame = tk.Frame(root)
button_frame.grid(row=0, column=0, padx=20, pady=20)

login_button = tk.Button(button_frame, text="Login", command=on_login)
login_button.grid(row=0, column=0, padx=10)

vote_button = tk.Button(button_frame, text="Vote", command=lambda: vote(selected_candidate.get()), state=tk.DISABLED)
vote_button.grid(row=0, column=1, padx=10)

results_button = tk.Button(button_frame, text="Results", command=show_results, state=tk.DISABLED)
results_button.grid(row=0, column=2, padx=10)

# Create login frame using grid()
login_frame = tk.Frame(root)

tk.Label(login_frame, text="Username:").grid(row=0, column=0, padx=5, pady=5)
username_entry = tk.Entry(login_frame)
username_entry.grid(row=0, column=1, padx=5, pady=5)

tk.Label(login_frame, text="Password:").grid(row=1, column=0, padx=5, pady=5)
password_entry = tk.Entry(login_frame, show="*")
password_entry.grid(row=1, column=1, padx=5, pady=5)

login_button = tk.Button(login_frame, text="Login", command=on_login)
login_button.grid(row=2, columnspan=2, padx=10, pady=10)

login_frame.grid(row=1, column=0, padx=20, pady=20)

# Create a frame to hold voting widgets
vote_frame = tk.Frame(root)

selected_candidate = tk.StringVar()
selected_candidate.set("Candidate A")  # Default selection

candidate_label = tk.Label(vote_frame, text="Select Candidate:")
candidate_label.grid(row=0, column=0, padx=5, pady=5)

candidate_menu = tk.OptionMenu(vote_frame, selected_candidate, *candidates.keys())
candidate_menu.grid(row=0, column=1, padx=5, pady=5)

vote_frame.grid_forget()  # Initially hide vote frame

root.mainloop()